let collection = [];

// Write the queue functions below.


// 1. Output all the elements of the queue

    function print (){
        return collection
    }

// 2. Adds element to the rear of the queue
    function enqueue(element){

        collection[collection.length] = element

        console.log(collection.length)
        return collection
    }

// 3. Removes element from the front of the queue
    function dequeue(element){
      collection.shift(element)
      return collection
    }

// 4. Show element at the front
    function front(){
        return collection[0]
    }

// 5. Show the total number of elements
    function size (){
        return collection.length
    }


// 6. Outputs a Boolean value describing whether queue is empty or not
    function isEmpty(){
       if (collection.length < 0){
        console.log(collection.length)
        return true
       } else {
            return false
       }
    }


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};